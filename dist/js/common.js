'use strict';
if(!window.console) window.console = {};
if(!window.console.memory) window.console.memory = function() {};
if(!window.console.debug) window.console.debug = function() {};
if(!window.console.error) window.console.error = function() {};
if(!window.console.info) window.console.info = function() {};
if(!window.console.log) window.console.log = function() {};

// sticky footer
//-----------------------------------------------------------------------------
if(!Modernizr.flexbox) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      noFlexboxStickyFooter = function() {
        $pageBody.height('auto');
        if($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
        } else {
          $pageWrapper.height('auto');
        }
      };
    $(window).on('load resize', noFlexboxStickyFooter);
  })();
}
if(ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
  (function(){
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      ieFlexboxFix = function() {
        if($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageWrapper.height($(window).height());
          $pageBody.removeClass('flex-none');
        } else {
          $pageWrapper.height('auto');
        }
      };
    ieFlexboxFix();
    $(window).on('load resize', ieFlexboxFix);
  })();
}

$(function() {

    $('input[placeholder], textarea[placeholder]').placeholder();

    function bannerSlider() {
        var slider = $('#banner-slider'),
            arrow = $('.js-slider-arrow');
        slider.slick({
            infinite: true,
            dots: false,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 400000,
            speed: 600,
            fade: true,
            cssEase: 'cubic-bezier(0.215, 0.610, 0.355, 1.000)',
        });
        arrow.on('click', function(e) {
            e.preventDefault();
            slider.slick($(this).attr('data-slider'));
        });
    }

    function reviewsSlider() {
        var slider = $('#reviews-slider'),
            arrow = $('.js-reviews-arrow');
        slider.slick({
            infinite: true,
            dots: false,
            arrows: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 543,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
            ]
        });
        arrow.on('click', function(e) {
            e.preventDefault();
            slider.slick($(this).attr('data-slider'));
        });
    }

    function maskedInput() {
        $("[type='tel']").mask("+38 (999) 999 99 99",{placeholder:"+38 (---) --- -- --"});
    }

    function bannerHeight() {
        $('.banner__info').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });
    }

    function headerForm() {
        var form = $('.header__form'),
            btn = $('.js-call'),
            close = $('.close-form');
        btn.on('click',function (e) {
            e.preventDefault();
            form.addClass('active');
        });
        close.on('click',function (e) {
            e.preventDefault();
            form.removeClass('active');
        });
    }

    function validateForm() {
        var form = $('form');
        form.each(function () {
            var thisForm = $(this);
            $(this).validate({
                rules: {
                    name: {
                        required: true
                    },
                    tel: {
                        required: true
                    }
                },
                submitHandler: function(form) {
                    thisForm.addClass('submit').text('Дякуємо за запис');
                    setTimeout(function () {
                        form.submit();
                    },4000);
                }
            });
        });
    }

    bannerSlider();
    maskedInput();
    reviewsSlider();
    bannerHeight();
    validateForm();
    headerForm();
});
